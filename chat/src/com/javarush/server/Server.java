package com.javarush.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * chat
 * 10.06.2021
 * Основной класс сервера
 *
 * @author Anastasiya Zhalnina
 */

public class Server {
    //ключ - имя клиента, значение - соединение с ним
    //Обеспечиваем потокобезопасность, используя ConcurrentHashMap, т.к. работа с этим полем происходит из разных потоков
    private static Map<String, Connection> connectionMap = new ConcurrentHashMap<>();

    /**
     * Отправка сообщение message всем соединениям из connectionMap
     *
     * @param message - отправляемое сообщение
     */
    public static void sendBroadcastMessage(Message message) {
        for (Connection connection : connectionMap.values()) {
            try {
                connection.send(message);
            } catch (IOException e) {
                ConsoleHelper.writeMessage("Сообщение не было отправлено.");
            }
        }
    }

    /**
     * Обработчик, в котором происходит обмен сообщениями с клиентом.
     * Реализует протокол общения с клиентом.
     */
    private static class Handler extends Thread {
        private Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        /**
         * Знакомство сервера с клиентом
         *
         * @param connection - соединение
         * @return - имя нового клиента
         * @throws IOException
         * @throws ClassNotFoundException
         */
        private String serverHandshake(Connection connection) throws IOException, ClassNotFoundException {
            Message answer;
            do {
                //команда запроса имени пользователя
                connection.send(new Message(MessageType.NAME_REQUEST));
                //получаем ответ клиента
                answer = connection.receive();
                //проверка: что получена команда с именем пользователя, что оно не пустое, и пользователь с таким именем еще не подключен
            } while (answer.getType() != MessageType.USER_NAME || answer.getData().isEmpty() || connectionMap.containsKey(answer.getData()));

            //добавляем нового пользователя и соединение с ним
            connectionMap.put(answer.getData(), connection);
            //отправляем команду, что имя принято
            connection.send(new Message(MessageType.NAME_ACCEPTED));
            //возвращаем имя нового клиента
            return answer.getData();
        }

        /**
         * Отправка клиенту (новому участнику) информации об остальных клиентах (участниках) чата
         *
         * @param connection - соединение с участником
         * @param userName   - имя участника
         * @throws IOException
         */
        private void notifyUsers(Connection connection, String userName) throws IOException {
            for (String name : connectionMap.keySet()) {
                if (!name.equals(userName)) {
                    //отправка сообщения о том, что был добавлен пользователь с именем name для каждого имени из connectionMap
                    connection.send(new Message(MessageType.USER_ADDED, name));
                }
            }
        }

        /**
         * Цикл обработки сообщений сервером
         *
         * @param connection - соединение с участником
         * @param userName   - имя участника
         * @throws IOException
         * @throws ClassNotFoundException
         */
        private void serverMainLoop(Connection connection, String userName) throws IOException, ClassNotFoundException {
            while (true) {
                //принимаем сообщение клиента
                Message answer = connection.receive();
                StringBuilder message = new StringBuilder();
                //если принято сообщение (тип TEXT)
                if (answer.getType() == MessageType.TEXT) {
                    //формируем новое текстовое сообщение
                    String text = message.append(userName).append(": ").append(answer.getData()).toString();
                    //отправляем сформированное сообщение всем клиентам
                    sendBroadcastMessage(new Message(MessageType.TEXT, text));
                } else {
                    ConsoleHelper.writeMessage("Сообщение не является текстом!");
                }
            }
        }

        @Override
        public void run() {
            ConsoleHelper.writeMessage("Установлено соединение с удаленным адресом" + socket.getRemoteSocketAddress().toString());
            String userName = null;
            try {
                //создаем соединение
                Connection connection = new Connection(socket);
                //знакомство сервера с клиентом, сохраняем имя нового клиента
                userName = serverHandshake(connection);
                //рассылаем всем участникам чата информацию об имени присоединившегося участника
                sendBroadcastMessage(new Message(MessageType.USER_ADDED, userName));
                //сообщаем новому участнику о существующих участниках
                notifyUsers(connection, userName);
                //запускаем главный цикл обработки сообщений
                serverMainLoop(connection, userName);
                //удаляем запись соответствующую userName
                connectionMap.remove(userName);
                //отправляем всем участникам чата сообщение о том, что текущий пользователь удален
                sendBroadcastMessage(new Message(MessageType.USER_REMOVED, userName));
            } catch (IOException | ClassNotFoundException e) {
                ConsoleHelper.writeMessage("Пользователь покинул чат.");
                if (userName != null) {
                    connectionMap.remove(userName);
                    sendBroadcastMessage(new Message(MessageType.USER_REMOVED, userName));
                }
                ConsoleHelper.writeMessage("Соединение с удаленным сервером закрыто.");
            }
        }
    }

    public static void main(String[] args) {
        //Создаем серверное сокетное соединение, запрашиваем порт сервера, используя ConsoleHelper
        ConsoleHelper.writeMessage("Введите порт соединения:");
        try (ServerSocket serverSocket = new ServerSocket(ConsoleHelper.readInt())) {
            System.out.println("Сервер запущен...");
            //Ожидаем подключения у сокету
            while (true) {
                //Создаем новый поток обработчик, в котором будет происходить обмен сообщениями с клиентом.
                new Handler(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.out.println("Произошла ошибка!");
        }
    }

}


