package com.javarush.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * chat
 * 10.06.2021
 * Вспомогательный класс, для чтения или записи в консоль
 *
 * @author Anastasiya Zhalnina
 */

public class ConsoleHelper {
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Вывод сообщения в консоль
     *
     * @param message - выводимое сообщение
     */
    public static void writeMessage(String message) {
        System.out.println(message);
    }

    /**
     * Считывание строки с консоли
     *
     * @return считанная строка
     */
    public static String readString() {
        while (true) {
            try {
                return bufferedReader.readLine();
            } catch (IOException e) {
                System.out.println("Произошла ошибка при попытки ввода текста. Попробуйте еще раз.");
            }
        }
    }

    /**
     * Считывание числа с консоли
     *
     * @return считанное числа
     */
    public static int readInt() {
        while (true) {
            try {
                return Integer.parseInt(readString());
            } catch (NumberFormatException e) {
                System.out.println("Произошла ошибка при попытки ввода числа. Попробуйте еще раз.");
            }
        }
    }
}
