package com.javarush.server;

import java.io.Serializable;

/**
 * chat
 * 10.06.2021
 * Класс, отвечающий за пересылаемые сообщения
 * Message - это данные, которые одна сторона отправляет, а вторая принимает
 *
 * @author Anastasiya Zhalnina
 */

public class Message implements Serializable {
    private final MessageType type; //тип сообщения
    private final String data;      //данные сообщения

    public Message(MessageType type) {
        this.type = type;
        this.data = null;
    }

    public Message(MessageType type, String data) {
        this.type = type;
        this.data = data;
    }

    public MessageType getType() {
        return type;
    }

    public String getData() {
        return data;
    }
}
