package com.javarush.server;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * chat
 * 10.06.2021
 * Класс соединения между клиентом и сервером - выполняет роль обертки над классом java.net.Socket,
 * которая умеет сериализовать и десериализовать объекты типа Message в сокет.
 *
 * @author Anastasiya Zhalnina
 */

public class Connection implements Closeable {
    private final Socket socket;
    private final ObjectOutputStream out;
    private final ObjectInputStream in;

    public Connection(Socket socket) throws IOException {
        this.socket = socket;
        this.out = new ObjectOutputStream(socket.getOutputStream());
        this.in = new ObjectInputStream(socket.getInputStream());
    }

    /**
     * Записывает(сериализует) сообщение message в ObjectOutputStream
     *
     * @param message - записываемое сообщение
     * @throws IOException
     */
    public void send(Message message) throws IOException {
        //Метод вызывается из нескольких потоков.
        //Обеспечиваем возможность записи в объект ObjectOutputStream только одним потоком в определенный момент времени,
        //остальные желающие ждали завершения записи.
        synchronized (out) {
            out.writeObject(message);
        }
    }

    /**
     * @return Чтение (десериализация) данных из ObjectInputStream
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Message receive() throws IOException, ClassNotFoundException {
        //Метод вызывается из нескольких потоков.
        //Обеспечиваем возможность чтения из объекта ObjectInputStream только одним потоком в определенный момент времени,
        //остальные желающие ждали завершения записи.
        synchronized (in) {
            return (Message) in.readObject();
        }
    }

    /**
     * Получение удаленного адреса сокетного соединения
     *
     * @return - удаленный адрес сокетного соединения
     */
    public SocketAddress getRemoteSocketAddress() {
        return socket.getRemoteSocketAddress();
    }

    /**
     * Закрытие всех ресурсов класса
     *
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        out.close();
        in.close();
        socket.close();
    }
}
