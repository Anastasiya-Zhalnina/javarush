package com.javarush.client;

/**
 * chat
 * 11.06.2021
 *
 * @author Anastasiya Zhalnina
 */

public class ClientGuiController extends Client {
    private ClientGuiModel model;
    private ClientGuiView view;

    public ClientGuiController() {
        this.model = new ClientGuiModel();
        this.view = new ClientGuiView(this);
    }

    @Override
    protected SocketThread getSocketThread() {
        return new GuiSocketThread();
    }

    @Override
    public void run() {
        SocketThread socketThread = getSocketThread();
        socketThread.run();
    }

    @Override
    protected String getServerAddress() {
        return view.getServerAddress();
    }

    @Override
    protected int getServerPort() {
        return view.getServerPort();
    }

    @Override
    protected String getUserName() {
        return view.getUserName();
    }

    /**
     * Получаем модель
     *
     * @return модель
     */
    public ClientGuiModel getModel() {
        return model;
    }

    public static void main(String[] args) {
        new ClientGuiController().run();
    }

    public class GuiSocketThread extends SocketThread {

        /**
         * Устанавливает новое сообщение у модели и вызывает обновление вывода сообщений у представления
         *
         * @param message - выводимый текст
         */
        @Override
        protected void processIncomingMessage(String message) {
            model.setNewMessage(message);
            view.refreshMessages();
        }

        /**
         * Добавляет нового пользователя в модель и вызывает обновление вывода пользователей у отображения
         *
         * @param userName - имя участника
         */
        @Override
        protected void informAboutAddingNewUser(String userName) {
            model.addUser(userName);
            view.refreshUsers();
        }

        /**
         * Удаляет пользователя из модели и вызывает обновление вывода пользователей у отображения.
         *
         * @param userName - имя участника
         */
        @Override
        protected void informAboutDeletingNewUser(String userName) {
            model.deleteUser(userName);
            view.refreshUsers();
        }

        @Override
        protected void notifyConnectionStatusChanged(boolean clientConnected) {
            view.notifyConnectionStatusChanged(clientConnected);
        }
    }
}
