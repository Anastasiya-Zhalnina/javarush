package com.javarush.client;

import com.javarush.server.ConsoleHelper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * chat
 * 10.06.2021
 * Бот
 *
 * @author Anastasiya Zhalnina
 */

public class BotClient extends Client {
    /**
     * Получаем объект BotSocketThread
     *
     * @return - объект BotSocketThread
     */
    @Override
    protected SocketThread getSocketThread() {
        return new BotSocketThread();
    }

    /**
     * @return false - мы не хотим, чтобы бот отправлял текст введенный в консоль
     */
    @Override
    protected boolean shouldSendTextFromConsole() {
        return false;
    }

    /**
     * Генерация имени Бота
     *
     * @return - имя бота
     */
    @Override
    protected String getUserName() {
        int X = (int) (Math.random() * 100);
        return "date_bot_" + X;
    }

    public class BotSocketThread extends SocketThread {
        /**
         * Отправляем сообщение
         *
         * @throws IOException
         * @throws ClassNotFoundException
         */
        @Override
        protected void clientMainLoop() throws IOException, ClassNotFoundException {
            sendTextMessage("Привет чатику. Я бот. Понимаю команды: дата, день, месяц, год, время, час, минуты, секунды.");
            super.clientMainLoop();
        }

        /**
         * Обрабатываем входящее сообщение
         *
         * @param message - выводимый текст
         */
        @Override
        protected void processIncomingMessage(String message) {
            //выводим в консоль текст полученного сообщения
            ConsoleHelper.writeMessage(message);
            if (message == null || !message.contains(": ")) return;
            String[] words = message.split(": ");
            //получаем имя пользователя из сообщения
            String userName = words[0];
            //получаем текст из сообщения
            String text = words[1];
            Calendar calendar = new GregorianCalendar();
            //отправляем ответ в зависимости от текста принятого сообщения
            if (text.equals("дата")) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("d.MM.yyy");
                sendTextMessage("Информация для " + userName + ": " + dateFormat.format(calendar.getTime()));
            }
            if (text.equals("день")) {
                SimpleDateFormat dayFormat = new SimpleDateFormat("d");
                sendTextMessage("Информация для " + userName + ": " + dayFormat.format(calendar.getTime()));
            }
            if (text.equals("месяц")) {
                SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM");
                sendTextMessage("Информация для " + userName + ": " + monthFormat.format(calendar.getTime()));
            }
            if (text.equals("год")) {
                SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
                sendTextMessage("Информация для " + userName + ": " + yearFormat.format(calendar.getTime()));
            }
            if (text.equals("время")) {
                SimpleDateFormat timeFormat = new SimpleDateFormat("H:mm:ss");
                sendTextMessage("Информация для " + userName + ": " + timeFormat.format(calendar.getTime()));
            }
            if (text.equals("час")) {
                SimpleDateFormat hourFormat = new SimpleDateFormat("H");
                sendTextMessage("Информация для " + userName + ": " + hourFormat.format(calendar.getTime()));
            }
            if (text.equals("минуты")) {
                SimpleDateFormat minutesFormat = new SimpleDateFormat("m");
                sendTextMessage("Информация для " + userName + ": " + minutesFormat.format(calendar.getTime()));
            }
            if (text.equals("секунды")) {
                SimpleDateFormat secondFormat = new SimpleDateFormat("s");
                sendTextMessage("Информация для " + userName + ": " + secondFormat.format(calendar.getTime()));
            }
        }
    }

    public static void main(String[] args) {
        new BotClient().run();
    }
}
