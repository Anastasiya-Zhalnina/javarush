package com.javarush.client;

import com.javarush.server.MessageType;
import com.javarush.server.Connection;
import com.javarush.server.ConsoleHelper;
import com.javarush.server.Message;

import java.io.IOException;
import java.net.Socket;

/**
 * chat
 * 10.06.2021
 *
 * @author Anastasiya Zhalnina
 */

public class Client {
    protected Connection connection;
    private volatile boolean clientConnected = false; //true - если клиент подсоединен к серверу, false в противном случае

    /**
     * Запрашивает ввод адреса сервера у пользователя
     *
     * @return - возвращает введенное значение (Адрес может быть строкой, содержащей ip или localhost)
     */
    protected String getServerAddress() {
        ConsoleHelper.writeMessage("Введите адрес сервера: ");
        return ConsoleHelper.readString();
    }

    /**
     * Запрашивает ввод порта сервера
     *
     * @return - порт сервера
     */
    protected int getServerPort() {
        ConsoleHelper.writeMessage("Введите порт сервера: ");
        return ConsoleHelper.readInt();
    }

    /**
     * Запрашивает имя пользователя
     *
     * @return - имя пользователя
     */
    protected String getUserName() {
        ConsoleHelper.writeMessage("Введите имя: ");
        return ConsoleHelper.readString();
    }

    //В данной реализации мы всегда отправляем текст введенный в консоль
    protected boolean shouldSendTextFromConsole() {
        return true;
    }

    /**
     * Создаем новый объект SocketThread
     *
     * @return - созданный объект SocketThread
     */
    protected SocketThread getSocketThread() {
        return new SocketThread();
    }

    /**
     * Создание нового текстового сообщения, используя переданный текст и отправка его серверу
     *
     * @param text - переданный текст
     */
    protected void sendTextMessage(String text) {
        try {
            connection.send(new Message(MessageType.TEXT, text));
        } catch (IOException e) {
            ConsoleHelper.writeMessage("Произошла ошибка, сообщение не отправлено.");
            clientConnected = false;
        }
    }

    public void run() {
        //создаем новый сокетный поток
        SocketThread socketThread = getSocketThread();
        //помечаем поток как daemon, для того, чтобы при выходе из программы вспомогательный поток прервался автоматически.
        socketThread.setDaemon(true);
        //запускаем вспомогательный поток
        socketThread.start();
        synchronized (this) {
            try {
                //заставляем текущий поток ожидать, пока он не получит нотификацию из другого потока
                this.wait();
            } catch (InterruptedException exception) {
                ConsoleHelper.writeMessage("При работе клиента возникла ошибка.");
            }
        }
        if (clientConnected) {
            ConsoleHelper.writeMessage("Соединение установлено. Для выхода наберите команду 'exit'.");
        } else {
            ConsoleHelper.writeMessage("Произошла ошибка во время работы клиента.");
        }
        //считываем сообщение с консоли пока клиент подключен
        while (clientConnected) {
            String message = ConsoleHelper.readString();
            if (message.equals("exit")) {
                return;
            }
            if (shouldSendTextFromConsole()) {
                //отправляем считанный текст
                sendTextMessage(message);
            }
        }
    }

    /**
     * Отвечает за поток, устанавливающий сокетное соединение и читающий сообщения сервера.
     */
    public class SocketThread extends Thread {
        /**
         * Выводит текст  message в консоль
         *
         * @param message - выводимый текст
         * @throws IOException
         */
        protected void processIncomingMessage(String message) throws IOException {
            ConsoleHelper.writeMessage(message);
        }

        /**
         * Вывод информации, что участник с именем userName присоединился к чату
         *
         * @param userName - имя участника
         */
        protected void informAboutAddingNewUser(String userName) {
            ConsoleHelper.writeMessage("Участник с именем " + userName + " присоединяется к чату.");
        }

        /**
         * Вывод в консоль информацию, что участник с именем userName покинул чат
         *
         * @param userName - имя участника
         */
        protected void informAboutDeletingNewUser(String userName) {
            ConsoleHelper.writeMessage("Участник с именем " + userName + " покинул чат.");
        }

        /**
         * Устанавливает значение поля clientConnected внешнего объекта Client в соответствии с переданным параметром.
         * Пробуждает ожидающий основной поток класса Client
         *
         * @param clientConnected - переданный параметр
         */
        protected void notifyConnectionStatusChanged(boolean clientConnected) {
            Client.this.clientConnected = clientConnected;
            synchronized (Client.this) {
                Client.this.notify();
            }
        }

        /**
         * Представляет клиента серверу
         *
         * @throws IOException
         * @throws ClassNotFoundException
         */
        protected void clientHandshake() throws IOException, ClassNotFoundException {
            while (true) {
                //получаем сообщение
                Message message = connection.receive();
                //если тип полученного сообщения (сервер запросил имя)
                if (message.getType() == MessageType.NAME_REQUEST) {
                    //принимаем имя клиента
                    String userName = getUserName();
                    //создаем новое сообщение и отправляем сообщение серверу
                    connection.send(new Message(MessageType.USER_NAME, userName));
                    //если тип полученного сообщения (сервер принял имя)
                } else if (message.getType() == MessageType.NAME_ACCEPTED) {
                    //сообщаем об этом главному потоку
                    notifyConnectionStatusChanged(true);
                    break;
                } else {
                    throw new IOException("Unexpected MessageType");
                }
            }
        }

        /**
         * Реализация главного цикла обработки сообщений сервера
         *
         * @throws IOException
         * @throws ClassNotFoundException
         */
        protected void clientMainLoop() throws IOException, ClassNotFoundException {
            while (true) {
                //получаем сообщение от сервера
                Message message = connection.receive();
                if (message.getType() == MessageType.TEXT) {
                    processIncomingMessage(message.getData());
                } else if (message.getType() == MessageType.USER_ADDED) {
                    informAboutAddingNewUser(message.getData());
                } else if (message.getType() == MessageType.USER_REMOVED) {
                    informAboutDeletingNewUser(message.getData());
                } else {
                    throw new IOException("Unexpected MessageType");
                }
            }
        }

        @Override
        public void run() {
            //запрашиваем адрес сервера
            String address = getServerAddress();
            //запрашиваем порт сервера
            int port = getServerPort();
            try {
                //создаем новый объект сокет
                Socket socket = new Socket(address, port);
                //создаем объект connection
                Connection connection = new Connection(socket);
                Client.this.connection = connection;
                //знакомство клиента с сервером
                clientHandshake();
                //основной цикл обработки сообщений сервера
                clientMainLoop();
            } catch (IOException | ClassNotFoundException e) {
                notifyConnectionStatusChanged(false);
            }
        }
    }

    public static void main(String[] args) {
        new Client().run();
    }
}
