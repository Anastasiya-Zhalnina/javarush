package com.javarush.client;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * chat
 * 11.06.2021
 *
 * @author Anastasiya Zhalnina
 */

public class ClientGuiModel {
    private final Set<String> allUserNames = new HashSet<>(); //все имена участников чата
    private String newMessage; //новое сообщение, которое получил клиент

    public Set<String> getAllUserNames() {
        return Collections.unmodifiableSet(allUserNames);
    }

    public void setNewMessage(String newMessage) {
        this.newMessage = newMessage;
    }

    public String getNewMessage() {
        return newMessage;
    }

    /**
     * Добавление имени участника во множество, хранящее всех участников
     *
     * @param newUserName - имя нового участника
     */
    public void addUser(String newUserName) {
        allUserNames.add(newUserName);
    }

    /**
     * Удаление имени участника из множества.
     *
     * @param userName - имя участника
     */
    public void deleteUser(String userName) {
        allUserNames.remove(userName);
    }
}
