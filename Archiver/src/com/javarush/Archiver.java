package com.javarush;

import com.javarush.exception.WrongZipFileException;

import java.io.IOException;

/**
 * Archiver
 * 14.07.2021
 * Архиватор
 *
 * @author Anastasiya Zhalnina
 */
public class Archiver {
    public static void main(String[] args) {
        Operation operation = null;
        while (operation != Operation.EXIT) {
            try {
                operation = askOperation();
                CommandExecutor.execute(operation);
            } catch (WrongZipFileException e) {
                ConsoleHelper.writeMessage("Вы не выбрали файл архива или выбрали неверный файл.");
            } catch (Exception e) {
                ConsoleHelper.writeMessage("Произошла ошибка. Проверьте введенные данные.");
            }
        }
    }

    public static Operation askOperation() throws IOException {
        ConsoleHelper.writeMessage("Выберите операцию:");
        ConsoleHelper.writeMessage(Operation.CREATE.ordinal() + " - упаковать файлы в архив");
        ConsoleHelper.writeMessage(Operation.ADD.ordinal() + " - добавить файл в архив");
        ConsoleHelper.writeMessage(Operation.REMOVE.ordinal() + " - удалить файл из архива");
        ConsoleHelper.writeMessage(Operation.EXTRACT.ordinal() + " - распаковать архив");
        ConsoleHelper.writeMessage(Operation.CONTENT.ordinal() + " - просмотреть содержимое архива");
        ConsoleHelper.writeMessage(Operation.EXIT.ordinal() + " - выход");
        int number = ConsoleHelper.readInt();
        if (number == Operation.CREATE.ordinal()) {
            return Operation.CREATE;
        } else if (number == Operation.ADD.ordinal()) {
            return Operation.ADD;
        } else if (number == Operation.REMOVE.ordinal()) {
            return Operation.REMOVE;
        } else if (number == Operation.EXTRACT.ordinal()) {
            return Operation.EXTRACT;
        } else if (number == Operation.CONTENT.ordinal()) {
            return Operation.CONTENT;
        } else if (number == Operation.EXIT.ordinal()) {
            return Operation.EXIT;
        } else
            return null;
    }
}
