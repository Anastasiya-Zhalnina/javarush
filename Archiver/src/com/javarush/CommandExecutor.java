package com.javarush;

import com.javarush.command.*;


import java.util.HashMap;
import java.util.Map;

/**
 * Archiver
 * 14.07.2021
 * Хранит объекты класса нужной команды
 * @author Anastasiya Zhalnina
 */
public class CommandExecutor {
    private static final Map<Operation, Command> ALL_KNOWN_COMMANDS_MAP = new HashMap<>();
    static {
        ALL_KNOWN_COMMANDS_MAP.put(Operation.CREATE, new ZipCreateCommand());
        ALL_KNOWN_COMMANDS_MAP.put(Operation.ADD, new ZipAddCommand());
        ALL_KNOWN_COMMANDS_MAP.put(Operation.REMOVE, new ZipRemoveCommand());
        ALL_KNOWN_COMMANDS_MAP.put(Operation.EXTRACT, new ZipExtractCommand());
        ALL_KNOWN_COMMANDS_MAP.put(Operation.CONTENT, new ZipContentCommand());
        ALL_KNOWN_COMMANDS_MAP.put(Operation.EXIT, new ExitCommand());
    }
    private CommandExecutor() {
    }

    public static void execute(Operation operation) throws Exception{
        for(Map.Entry<Operation, Command> pair: ALL_KNOWN_COMMANDS_MAP.entrySet()){
            Operation key = pair.getKey();
            Command value = pair.getValue();
            if(operation.equals(key)){
                value.execute();
            }
        }
    }
}
