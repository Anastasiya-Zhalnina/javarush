package com.javarush;


/**
 * Archiver
 * 15.07.2021
 * Класс, отвечающий за свойства каждого файла (имя файла, размер файла до и после сжатия, метод сжатия)
 *
 * @author Anastasiya Zhalnina
 */
public class FileProperties {
    private String name;
    private long size;
    private long compressedSize;
    private int compressionMethod;

    public FileProperties(String name, long size, long compressedSize, int compressionMethod) {
        this.name = name;
        this.size = size;
        this.compressedSize = compressedSize;
        this.compressionMethod = compressionMethod;
    }

    //считает степень сжатия
    public long getCompressionRatio() {
        return 100 - ((compressedSize * 100) / size);
    }

    @Override
    public String toString() {

        if (size > 0) {
            return name + " " + (size/1024) + " Kb(" + (compressedSize/1024) + "Kb) сжатие: " + getCompressionRatio() + "%";
        } else
            return name;
    }

    public String getName() {
        return name;
    }

    public long getSize() {
        return size;
    }

    public long getCompressedSize() {
        return compressedSize;
    }

    public int getCompressionMethod() {
        return compressionMethod;
    }
}
