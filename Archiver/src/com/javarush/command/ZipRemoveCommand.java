package com.javarush.command;

import com.javarush.ConsoleHelper;
import com.javarush.ZipFileManager;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Archiver
 * 14.07.2021
 * Команда удаления файла из архива
 *
 * @author Anastasiya Zhalnina
 */
public class ZipRemoveCommand extends ZipCommand {
    @Override
    public void execute() throws Exception {
            ConsoleHelper.writeMessage("Удаление файла из архива.");
            ZipFileManager zipFileManager = getZipFileManager();
            ConsoleHelper.writeMessage("Введите имя файла, который следует удалить из архива:");
            Path directory = Paths.get(ConsoleHelper.readString());
            zipFileManager.removeFile(directory);
            ConsoleHelper.writeMessage("Удаление было завершено.");

    }
}
