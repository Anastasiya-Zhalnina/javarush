package com.javarush.command;

import com.javarush.ConsoleHelper;
import com.javarush.FileProperties;
import com.javarush.ZipFileManager;

import java.util.List;

/**
 * Archiver
 * 14.07.2021
 * Команда просмотра содержимого архива
 * @author Anastasiya Zhalnina
 */
public class ZipContentCommand extends ZipCommand{
    @Override
    public void execute() throws Exception {
        ConsoleHelper.writeMessage("Просмотр содержимого архива.");
        ZipFileManager zipFileManager = getZipFileManager();
        ConsoleHelper.writeMessage("Содержимое архива:");
        List<FileProperties> list = zipFileManager.getFilesList();
        for(FileProperties properties: list){
            ConsoleHelper.writeMessage(properties.toString());
        }
        ConsoleHelper.writeMessage("Содержимое архива прочитано.");
    }
}
