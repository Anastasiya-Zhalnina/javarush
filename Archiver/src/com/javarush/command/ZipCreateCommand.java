package com.javarush.command;

import com.javarush.ConsoleHelper;
import com.javarush.ZipFileManager;
import com.javarush.exception.PathIsNotFoundException;

import java.nio.file.Paths;

/**
 * Archiver
 * 14.07.2021
 * Команда создания архива (упаковки файлов в архив)
 *
 * @author Anastasiya Zhalnina
 */
public class ZipCreateCommand extends ZipCommand {
    @Override
    public void execute() throws Exception {
        try {
            ConsoleHelper.writeMessage("Создание архива.");
            ZipFileManager zipFileManager = getZipFileManager();
            ConsoleHelper.writeMessage("Введите путь к файлу который требуется заархивировать: ");
            String pathFile = ConsoleHelper.readString();
            zipFileManager.createZip(Paths.get(pathFile));
            ConsoleHelper.writeMessage("Архив создан.");
        } catch (PathIsNotFoundException e) {
            ConsoleHelper.writeMessage("Вы неверно указали имя файла или директории.");
        }
    }
}
