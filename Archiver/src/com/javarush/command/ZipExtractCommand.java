package com.javarush.command;

import com.javarush.ConsoleHelper;
import com.javarush.ZipFileManager;
import com.javarush.exception.WrongZipFileException;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Archiver
 * 14.07.2021
 * Команда распаковки архива
 * @author Anastasiya Zhalnina
 */
public class ZipExtractCommand extends ZipCommand{
    @Override
    public void execute() throws Exception {
        try {
            ConsoleHelper.writeMessage("Распаковка архива.");
            ZipFileManager zipFileManager = getZipFileManager();
            ConsoleHelper.writeMessage("Введите полное имя директории, в которую будет распакован архив:");
            Path directory = Paths.get(ConsoleHelper.readString());
            zipFileManager.extractAll(directory);
            ConsoleHelper.writeMessage("Архив распакован.");
        } catch (WrongZipFileException e) {
            ConsoleHelper.writeMessage("Вы неверно указали имя файла или директории.");
        }
    }
}
