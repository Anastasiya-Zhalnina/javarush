package com.javarush.command;

import com.javarush.ConsoleHelper;
import com.javarush.ZipFileManager;
import com.javarush.exception.PathIsNotFoundException;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Archiver
 * 14.07.2021
 * Команда добавления файла в архив
 * @author Anastasiya Zhalnina
 */
public class ZipAddCommand extends ZipCommand{
    @Override
    public void execute() throws Exception {
        try{
            ConsoleHelper.writeMessage("Добавление файла в архив.");
            ZipFileManager zipFileManager = getZipFileManager();
            ConsoleHelper.writeMessage("Введите путь к файлу, который необходимо добавить в архив:");
            Path directory = Paths.get(ConsoleHelper.readString());
            zipFileManager.addFile(directory);
            ConsoleHelper.writeMessage("Файл был добавлен.");
        } catch (PathIsNotFoundException e) {
            ConsoleHelper.writeMessage("Вы неверно указали имя файла или директории.");
        }
    }
}
