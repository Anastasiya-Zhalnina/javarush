package com.javarush.command;

import com.javarush.ConsoleHelper;

/**
 * Archiver
 * 14.07.2021
 *
 * @author Anastasiya Zhalnina
 */
public class ExitCommand implements Command{
    @Override
    public void execute() throws Exception {
        ConsoleHelper.writeMessage("До встречи!");
    }
}
