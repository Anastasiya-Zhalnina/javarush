package com.javarush.command;

/**
 * Archiver
 * 14.07.2021
 *
 * @author Anastasiya Zhalnina
 */
public interface Command {
    void execute() throws Exception;
}
