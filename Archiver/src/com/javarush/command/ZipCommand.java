package com.javarush.command;

import com.javarush.ConsoleHelper;
import com.javarush.ZipFileManager;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Archiver
 * 14.07.2021
 * Команды, которые работают непосредственно с архивом
 *
 * @author Anastasiya Zhalnina
 */
public abstract class ZipCommand implements Command {
    public ZipFileManager getZipFileManager() throws Exception {
        ConsoleHelper.writeMessage("Введите полный путь архива: ");
        String path = ConsoleHelper.readString();
        Path pathArchiver = Paths.get(path);
        return new ZipFileManager(pathArchiver);
    }
}

