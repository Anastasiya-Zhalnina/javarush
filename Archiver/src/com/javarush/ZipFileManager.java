package com.javarush;

import com.javarush.exception.PathIsNotFoundException;
import com.javarush.exception.WrongZipFileException;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;



/**
 * Archiver
 * 14.07.2021
 * Менеджер архива - совершает операции над файлом архива (файлом, который будет храниться на диске и иметь расширение zip)
 *
 * @author Anastasiya Zhalnina
 */
public class ZipFileManager {
    private Path zipFile; // полный путь к архиву

    public ZipFileManager(Path zipFile) {
        this.zipFile = zipFile;
    }

    public void createZip(Path source) throws Exception {
        if (!Files.exists(zipFile.getParent())) {
            Files.createDirectories(zipFile.getParent());
        }
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(Files.newOutputStream(zipFile))) {
            if (Files.isRegularFile(source)) {
                addNewZipEntry(zipOutputStream, source.getParent(), source.getFileName());
            } else if (Files.isDirectory(source)) {
                FileManager fileManager = new FileManager(source);
                List<Path> fileNames = fileManager.getFileList();
                for (Path fileName : fileNames) {
                    addNewZipEntry(zipOutputStream, source, fileName);
                }
            }
            if (!Files.isDirectory(source) && !Files.isRegularFile(source)) throw new PathIsNotFoundException();
        }
    }

    private void addNewZipEntry(ZipOutputStream zipOutputStream, Path filePath, Path fileName) throws Exception {
        try (InputStream inputStream = Files.newInputStream(filePath.resolve(fileName))) {
            ZipEntry zipEntry = new ZipEntry(fileName.toString());
            zipOutputStream.putNextEntry(zipEntry);
            copyData(inputStream, zipOutputStream);
            zipOutputStream.closeEntry();
        }
    }

    private void copyData(InputStream in, OutputStream out) throws Exception {
        int b;
        while ((b = in.read()) != -1) {
            out.write(b);
        }
    }

    public List<FileProperties> getFilesList() throws Exception {
        if (!Files.isRegularFile(zipFile)) {
            throw new WrongZipFileException();
        }
        List<FileProperties> list = new ArrayList<>();
        try (ZipInputStream zipInputStream = new ZipInputStream(Files.newInputStream(zipFile))) {
            ZipEntry zipEntry;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                copyData(zipInputStream, out);
                list.add(new FileProperties(zipEntry.getName(), zipEntry.getSize(), zipEntry.getCompressedSize(), zipEntry.getMethod()));
            }
            zipInputStream.closeEntry();
        }
        return list;
    }

    public void extractAll(Path outputFolder) throws Exception {
        if (!Files.isRegularFile(zipFile)) throw new WrongZipFileException();
        if (Files.notExists(outputFolder)) Files.createDirectories(outputFolder);
        try (ZipInputStream zipInputStream = new ZipInputStream(Files.newInputStream(zipFile));) {
            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                Path pathFile = outputFolder.resolve(entry.getName());
                if (Files.notExists(pathFile.getParent())) Files.createDirectories(pathFile.getParent());
                try (OutputStream ous = Files.newOutputStream(pathFile)) {
                    copyData(zipInputStream, ous);
                }
                zipInputStream.closeEntry();
            }
        }
    }

    public void removeFiles(List<Path> pathList) throws Exception {
        if (!Files.isRegularFile(zipFile)) throw new WrongZipFileException();
        Path tempZip = Files.createTempFile("temp", ".zip");
        try (ZipInputStream zipInputStream = new ZipInputStream(Files.newInputStream(zipFile));
             ZipOutputStream zipOutputStream = new ZipOutputStream(Files.newOutputStream(tempZip))) {
            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                Path path = Paths.get(entry.getName());
                if (pathList.contains(path)) {
                    ConsoleHelper.writeMessage("Файл удален: " + path);
                } else {
                    zipOutputStream.putNextEntry(entry);
                    copyData(zipInputStream, zipOutputStream);
                    zipInputStream.closeEntry();
                    zipOutputStream.closeEntry();
                }
            }
            Files.move(tempZip, zipFile, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    public void removeFile(Path path) throws Exception {
        removeFiles(Collections.singletonList(path));
    }

    public void addFiles(List<Path> absolutePathList) throws Exception {
        if (!Files.isRegularFile(zipFile)) throw new WrongZipFileException();
        Path tempZip = Files.createTempFile("temp", ".zip");
        List<Path> list = new ArrayList<>();
        try (ZipInputStream zipInputStream = new ZipInputStream(Files.newInputStream(zipFile));
             ZipOutputStream zipOutputStream = new ZipOutputStream(Files.newOutputStream(tempZip))) {
            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                zipOutputStream.putNextEntry(new ZipEntry(entry.getName()));
                copyData(zipInputStream, zipOutputStream);
                zipInputStream.closeEntry();
                zipOutputStream.closeEntry();
                list.add(Paths.get(entry.getName()));
            }
            for (Path path : absolutePathList) {
                if (Files.notExists(path)) throw new PathIsNotFoundException();
                if (!list.contains(path.getFileName())) {
                    addNewZipEntry(zipOutputStream, path.getParent(), path.getFileName());
                    ConsoleHelper.writeMessage("Файл " + path.getFileName() + " добавлен");
                } else {
                    ConsoleHelper.writeMessage("Файл " + path.getFileName() + " уже есть в архиве.");
                }
            }
            Files.move(tempZip, zipFile, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    public void addFile(Path absolutePath) throws Exception{
        addFiles(Collections.singletonList(absolutePath));
    }
}

