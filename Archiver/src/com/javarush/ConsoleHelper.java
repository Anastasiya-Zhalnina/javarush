package com.javarush;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Archiver
 * 14.07.2021
 *
 * @author Anastasiya Zhalnina
 */
public class ConsoleHelper {
    public static void writeMessage(String message){
        System.out.println(message);
    }

    public static String readString(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String string = null;
        try {
            string = reader.readLine();
        } catch (IOException e) {
            System.err.println("Ошибка ввода!");
        }
        return string;
    }

    public static int readInt(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int number = 0;
        try {
            number = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            System.err.println("Ошибка ввода, строка не может быть преобразована в число!");
        }
        return number;
    }
}
