package com.javarush.actions;

import com.javarush.View;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * HTML
 * 23.09.2021
 * Класс отмены действия
 *
 * @author Anastasiya Zhalnina
 */

public class UndoAction extends AbstractAction {
    private View view;

    public UndoAction(View view) {
        this.view = view;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        view.undo();
    }
}
