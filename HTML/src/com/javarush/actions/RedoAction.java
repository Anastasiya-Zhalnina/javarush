package com.javarush.actions;

import com.javarush.View;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * HTML
 * 23.09.2021
 * Класс возврата действия
 *
 * @author Anastasiya Zhalnina
 */

public class RedoAction extends AbstractAction {
    private View view;

    public RedoAction(View view) {
        this.view = view;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        view.redo();
    }
}
