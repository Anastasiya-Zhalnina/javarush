package com.javarush;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * HTML
 * 23.09.2021
 *
 * @author Anastasiya Zhalnina
 */

public class HTMLFileFilter extends FileFilter {
    @Override
    public boolean accept(File file) {
        return (file.getName().toLowerCase().endsWith(".htm") || file.getName().toLowerCase().endsWith(".html")) || file.isDirectory();
    }

    @Override
    public String getDescription() {
        return "HTML и HTM файлы";
    }
}
