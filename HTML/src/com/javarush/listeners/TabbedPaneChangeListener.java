package com.javarush.listeners;

import com.javarush.View;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * HTML
 * 23.09.2021
 * Слушает и обрабатывает изменения состояния панели вкладок
 *
 * @author Anastasiya Zhalnina
 */

public class TabbedPaneChangeListener implements ChangeListener {
    private View view;

    public TabbedPaneChangeListener(View view) {
        this.view = view;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        view.selectedTabChanged();
    }
}
