package com.javarush.listeners;

import com.javarush.View;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * HTML
 * 23.09.2021
 *
 * @author Anastasiya Zhalnina
 */

public class FrameListener extends WindowAdapter {
    private View view;

    public FrameListener(View view) {
        this.view = view;
    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        super.windowClosing(windowEvent);
        view.exit();
    }
}
